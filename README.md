5. Dado el siguiente código el cual está en Java 8 en los métodos getEmployeeNameWithAge
y getEmployeeNameById se hace falta la expresión XPATH para realizar la búsqueda dentro
del XML y va dentro de la línea que contiene xpath.compile(“”) en el caso de la función
getEmployeeNameWithAge para buscar a los empleados que sean mayor al parámetro
edad y para la función getEmployeeNameById que encuentre al empleado que contengo el
atributo id igual al parámetro id de la misma función.