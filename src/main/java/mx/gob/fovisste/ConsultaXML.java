package mx.gob.fovisste;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * @author Eder Sam
 *
 */
public class ConsultaXML {

	public static void main(String[] args) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder;
		Document doc = null;
		try {
			builder = factory.newDocumentBuilder();
			doc = builder.parse("src/main/resources/employees.xml");
			// Create XPathFactory object
			XPathFactory xpathFactory = XPathFactory.newInstance();
			// Create XPath object
			XPath xpath = xpathFactory.newXPath();
			String name = getEmployeeNameById(doc, xpath, 4);
			System.out.println("Employee Name with ID 4: " + name);
			List<String> names = getEmployeeNameWithAge(doc, xpath, 30);
			System.out.println("Employees with 'age>30' are:" + Arrays.toString(names.toArray()));
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	private static List<String> getEmployeeNameWithAge(Document doc, XPath xpath, int age) {
		List<String> list = new ArrayList<>();
		try {
			XPathExpression expr = xpath.compile("/Employees/Employee");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					String name = element.getElementsByTagName("name").item(0).getTextContent();
					int employeeAge = Integer.valueOf(element.getElementsByTagName("age").item(0).getTextContent())
							.intValue();
					if (employeeAge > age) {
						list.add(name);
					}
				}
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

	private static String getEmployeeNameById(Document doc, XPath xpath, int id) {
		String name = null;
		try {
			XPathExpression expr = xpath.compile("/Employees/Employee[@id=" + id + "]/name");
			name = (String) expr.evaluate(doc, XPathConstants.STRING);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return name;
	}

}
